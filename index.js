const express = require('express');
const axios = require('axios');
const cheerio = require('cheerio');
const newspapers = require('./newspapers-list.js');

const PORT = process.env.PORT || 8000;

const app = express();
const allArticles = [];

const fetchArticlesForNewspaper = async (newspaper) => {
    const newspaperArticles = [];
    try {
        const response = await axios.get(newspaper.url);
        const html = response.data;

        const $ = cheerio.load(html);
        $('a:contains("climate")', html).each(function () {
            const title = $(this).text();
            const url = $(this).attr('href');
            newspaperArticles.push({
                newspaper: newspaper.name,
                title: title.trim(),
                url: newspaper.base + url
            });
        });
    } catch (error) {
        console.error(error);
    }

    return newspaperArticles;
};

newspapers.forEach(async (newspaper) => {
    const fetchedArticles = await fetchArticlesForNewspaper(newspaper);
    allArticles.push(...fetchedArticles);
});

app.get('/', (req, res) => {
    res.json('Welcome to my climate change news API');
});

app.get('/newspapers', async (req, res) => {
    res.json(newspapers.map(np => ({
        name: np.name,
        url: np.url
    })));
});

app.get('/news', async (req, res) => {
    res.json(allArticles);
});

app.get('/news/:newspaperId', async (req, res) => {
    const newspaperId = req.params.newspaperId;
    const newspaper = newspapers.filter(np => np.name == newspaperId)[0];

    const newspaperArticles = [];
    const fetchedArticles = await fetchArticlesForNewspaper(newspaper);
    newspaperArticles.push(...fetchedArticles);

    res.json(newspaperArticles);
});

app.listen(PORT, () => { console.log(`Server running on http://localhost:${PORT}`) });