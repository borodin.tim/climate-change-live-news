# Simple Climate Change Live News API

## Endpoints:
### `newspapers `
List of newspapers
### `news` 
List of all news articles
### `news/:newspaperId`
List of news from a certain newspaper